#ifndef _MAIN_H_
#define _MAIN_H_

#include <xc.h> // C:\Program Files\Microchip\xc8\v2.36\pic\include
#include <pic16f628a.h>
#include <stdint.h> 

#define LED_TRIS TRISAbits.TRISA1
#define LED_PORT PORTAbits.RA1

#define PS2_CLK_TRIS TRISBbits.TRISB0
#define PS2_CLK_PORT PORTBbits.RB0

#define PS2_DAT_TRIS TRISBbits.TRISB1
#define PS2_DAT_PORT PORTBbits.RB1


//void app(void);
//void delay(uint16_t iterations);

#endif /* _MAIN_H_ */