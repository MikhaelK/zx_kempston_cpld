#include "main.h"

// Uncalibrated delay, just waits a number of for-loop iterations
void delay(uint16_t iterations)
{
    //uint16_t i;
    for (uint16_t i = 0; i < iterations; i++) {
        // Prevent this loop from being optimized away.
        __asm("nop");
    }
}

void app(void)
{
    LED_PORT = 1; // LED On
    delay(30000); // ~500ms @ 4MHz
    LED_PORT = 0; // LED Off
    delay(30000); // ~500ms @ 4MHz
}