#include "main.h"

/*  see file:///C:/Program%20Files/Microchip/xc8/v2.36/docs/chips/16f628a.html
INTOSC oscillator: I/O function on RA6/OSC2/CLKOUT pin, I/O function on RA7/OSC1/CLKIN
Watchdog Timer Enable bit (WDTE) disabled
Power-up Timer Enable bit (PWRTE) disabled
RA5/MCLR/VPP pin function is MCLR
Brown-out Detect Enable bit (BOREN) enabled
Low-Voltage Programming Enable bit (LVP) RB4/PGM pin has PGM function, low-voltage programming enabled
Data memory code protection off
Code protection off   */
#pragma config FOSC = INTOSCIO, WDTE = OFF, PWRTE = OFF, MCLRE = ON, BOREN = ON, LVP = OFF, CPD = OFF, CP = OFF

void delay(uint16_t iterations);

// Сырые данные от ps/2.
volatile uint8_t     ps2_data_raw = 0x55;
// СЧетчик принятых битов от ps/2.
uint8_t     ps2_cnt;

void main(void)
{
    // Включим прерывания.
    INTCONbits.GIE = 1;
    // Прерывание на спадающем фронте RB0/INT.
    OPTION_REGbits.INTEDG = 0;
    // Настройки прерывания на RB0/INT.
    INTCONbits.INTE = 1;


    // Входы для ps/2.
    PS2_CLK_TRIS    = 1; // Pin as input
    PS2_DAT_TRIS    = 1; // Pin as input

    /*TXSTAbits.SYNC = 1;
    RCSTAbits.SPEN = 1;
    TXSTAbits.CSRC = 0;
    RCSTAbits.RX9   = 1;
    RCSTAbits.CREN  = 1;
    TXSTAbits.TXEN = 0;
    PIE1bits.RCIE = 1;*/
    
    LED_TRIS = 0; // Pin as output
    LED_PORT = 0; // LED off



    
    
    while (1) {
     
        LED_PORT = ps2_data_raw & 0b00000001;
        delay(300);
        LED_PORT = ps2_data_raw & 0b00000010;
        delay(300);
        LED_PORT = ps2_data_raw & 0b00000100;
        delay(300);
        LED_PORT = ps2_data_raw & 0b00001000;
        delay(300);
        LED_PORT = ps2_data_raw & 0b00010000;
        delay(300);
        LED_PORT = ps2_data_raw & 0b00100000;
        delay(300);
        LED_PORT = ps2_data_raw & 0b01000000;
        delay(300);
        LED_PORT = ps2_data_raw & 0b10000000;
        delay(300);
        delay(3000);
        ps2_cnt = 0;
    }
}

void __interrupt () ISR(){
    if(INTE && INTF) {
        //LED_PORT = PS2_DAT_PORT;
        ps2_data_raw |= (PS2_DAT_PORT<<(ps2_cnt++%7));
    INTF = 0;
    }

    /*if(PIR1bits.RCIF){
        //PIR1bits.RCIF = 0;
        ps2_data_raw = RCREG;
        //LED_PORT = ~LED_PORT;
    }*/
}

// Uncalibrated delay, just waits a number of for-loop iterations
void delay(uint16_t iterations)
{
    //uint16_t i;
    for (uint16_t i = 0; i < iterations; i++) {
        // Prevent this loop from being optimized away.
        __asm("nop");
    }
}