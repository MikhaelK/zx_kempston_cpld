module spi_to_zx_kempston_mouse // EPMx032 @ plcc44
(
	// Шина адреса Z80.
	input [15:0]ADR, // используются A0, A1, A5, A7, A8, A10, A15 
	// Шина данных Z80.
	output [7:0] DATA,
	// Управляющие сигналы Z80.
	input IORQ,
	input RD,
	// SPI для загрузки данных.
	input SPI_CLK,
	input SPI_NSS,
	input SPI_MOSI,
	// Сигнал запрета загрузки.
	output MOUSE_BUSY
);
spi_zxkm spi_zxkm_(ADR, DATA, IORQ, RD, SPI_CLK, SPI_NSS, SPI_MOSI, MOUSE_BUSY);
endmodule