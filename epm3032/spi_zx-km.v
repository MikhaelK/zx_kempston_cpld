	module spi_zxkm // EPMx032 @ plcc44
(
	// Шина адреса Z80.
	input [15:0]ADR, // используются A0, A1, A5, A7, A8, A10, A15 
	// Шина данных Z80.
	output [7:0] DATA,
	// Управляющие сигналы Z80.
	input IORQ,
	input RD,
	// SPI для загрузки данных.
	input SPI_CLK,
	input SPI_NSS,
	input SPI_MOSI,
	// Сигнал запрета загрузки.
	output MOUSE_BUSY
);
	// SPI
	reg [21:0] spi_mouse;
	wire spi_edge = SPI_CLK & (~SPI_NSS);
	always @(posedge spi_edge) begin	
			spi_mouse = spi_mouse << 1;
			spi_mouse[0] = SPI_MOSI;
	end
	wire mouse_io = IORQ | RD | ADR[5] | ~ADR[7] | ~ADR[0] | ~ADR[1] | ~ADR[15];
	wire mouse_ios = ~SPI_NSS | mouse_io;
	wire mouse_b = ~(mouse_ios |  ADR[8] |  ADR[10]);
	wire mouse_x = ~(mouse_ios | ~ADR[8] |  ADR[10]);
	wire mouse_y = ~(mouse_ios | ~ADR[8] | ~ADR[10]);
	
	//assign DATA = (mouse_b) ? 8'b11001100 : (8'hZ);
	//assign DATA = (mouse_x) ? 8'b11110001 : (8'hZ);
	//assign DATA = (mouse_y) ? 8'b11011110 : (8'hZ);
	
	assign DATA = (mouse_b) ? ({spi_mouse[5:2], 2'b11, spi_mouse[1:0]}) : (8'hZ);
	assign DATA = (mouse_x) ? (spi_mouse[13:6]) : (8'hZ);
	assign DATA = (mouse_y) ? (spi_mouse[21:14]) : (8'hZ);
	
	// Сигнал для запрета загрузки данных.
	assign MOUSE_BUSY = mouse_io;
endmodule
/*
                        KEMPSTON MOUSE.
PORT A15 A14 A13 A12 A11 A1O A9 A8 A7 A6 A5 A4 A3 A2 A1 A0 NOTE
FADF  1   1   1   1   1   0* 1  0* 1* 1  0* 1  1  1  1  1 Button
FBDF  1   1   1   1   1   0  1  1  1  1  0  1  1  1  1  1 Xкоорд
FFDF  1   1   1   1   1   1  1  1  1  1  0  1  1  1  1  1 Yкоорд
PORT A15 A14 A13 A12 A11 A1O A9 A8 A7 A6 A5 A4 A3 A2 A1 A0
#FADF — порт кнопок и (по отечественному стандарту) колёсика.
			D0: левая кнопка (0=нажата)
			D1: правая кнопка (0=нажата)
			D2: средняя кнопка (0=нажата)
			D3: зарезервировано под ещё одну кнопку (0=нажата)
			D4-D7: координата колёсика
#FBDF — X-координата (растёт слева направо)
#FFDF — Y-координата (растёт снизу вверх)
*/
