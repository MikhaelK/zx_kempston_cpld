module test_zx_km_port_decode;

reg [15:0]adr_;
reg iorq_;
reg rd_;

wire km_b_;
wire km_x_;
wire km_y_;

//устанавливаем экземпляр тестируемого модуля
zx_km_port_decode zx_km_port_decode_inst(
  .adr(adr_),
  .iorq(iorq_),
  .rd(rd_),
  .km_b(km_b_),
  .km_x(km_x_),
  .km_y(km_y_)
);

//моделируем сигнал тактовой частоты
//always
  //#10 iorq_ = ~iorq_;

//от начала времени...
initial
  begin
    iorq_ = 1;
    #1;
    rd_ = 1;
    #1;
    adr_ = 16'h0000;
    #1;
    iorq_ = 0;
    #1;
    rd_ = 0;
    #1;
    adr_ = 16'h0000;
    #1;
    iorq_ = 0;
    #1;
    rd_ = 0;
    #1;
    adr_ = 16'hfadf;
    #1;
    iorq_ = 1;
    #1;
    rd_ = 0;
    #1;
    adr_ = 16'hfadf;
    #1;
    iorq_ = 0;
    #1;
    rd_ = 1;
    #1;
    adr_ = 16'hfadf;
    #1;
    iorq_ = 0;
    #1;
    rd_ = 0;
    #1;
    adr_ = 16'hfbdf;
    #1;
    iorq_ = 1;
    #1;
    rd_ = 0;
    #1;
    adr_ = 16'hfbdf;
    #1;
    iorq_ = 0;
    #1;
    rd_ = 1;
    #1;
    adr_ = 16'hfbdf;
end

//заканчиваем симуляцию в момент времени "400"
initial
begin
  #400 $finish;
end

//создаем файл VCD для последующего анализа сигналов
initial
begin
  $dumpfile("zx_km_port_decode.vcd");
  $dumpvars(0,zx_km_port_decode_inst);
end

//наблюдаем на некоторыми сигналами системы
initial
$monitor($stime,, iorq_,, rd_,,, adr_,, km_b_,, km_x_);

endmodule
