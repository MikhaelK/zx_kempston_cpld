/* При дешифрации кемпстон мышки используються используются 
биты адресной шины A0, A1, A5, A7, A8, A10, A15 
                        KEMPSTON MOUSE.
PORT A15 A14 A13 A12 A11 A1O A9 A8 A7 A6 A5 A4 A3 A2 A1 A0 NOTE
FADF  1   1   1   1   1   0* 1  0* 1* 1  0* 1  1  1  1  1 Button
FBDF  1   1   1   1   1   0  1  1  1  1  0  1  1  1  1  1 Xкоорд
FFDF  1   1   1   1   1   1  1  1  1  1  0  1  1  1  1  1 Yкоорд
PORT A15 A14 A13 A12 A11 A1O A9 A8 A7 A6 A5 A4 A3 A2 A1 A0
#FADF — порт кнопок и (по отечественному стандарту) колёсика.
            D0: левая кнопка (0=нажата)
            D1: правая кнопка (0=нажата)
            D2: средняя кнопка (0=нажата)
            D3: зарезервировано под ещё одну кнопку (0=нажата)
            D4-D7: координата колёсика
#FBDF — X-координата (растёт слева направо)
#FFDF — Y-координата (растёт снизу вверх) */
module zx_km_port_decode (
    input   [15:0]adr,
    input   iorq,
    input   rd,
    output  km_b,
    output  km_x,
    output  km_y
);
    wire km_io_hard = ~adr[0] | ~adr[1] | ~adr[15];
    wire km_io = iorq | rd | adr[5] | ~adr[7] | km_io_hard;
    wire km_b = ~(km_io |  adr[8] |  adr[10]);
    wire km_x = ~(km_io | ~adr[8] |  adr[10]);
    wire km_y = ~(km_io | ~adr[8] | ~adr[10]);
endmodule
