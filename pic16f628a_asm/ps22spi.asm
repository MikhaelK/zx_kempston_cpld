;******************************************************************************
; Processor: PIC16F628 at 4 MHz using internal RC oscillator
; Function:  PS2 to SPI
; Filename:  ps22spi.asm
; Author:    Michael Kaa
;******************************************************************************
        LIST P=16F628A, R=DEC    ; Use the PIC16F628 and decimal system

        #include "P16F628A.INC"  ; Include header file

        __config  _INTRC_OSC_NOCLKOUT & _LVP_OFF & _WDT_OFF & _PWRTE_ON & _BODEN_ON

        CBLOCK 0x20             ; Declare variable addresses starting at 0x20
          Loop1, Loop2, zx_buttons, zx_cord_x, zx_cord_y, w_temp, status_temp, ps_data, ps_cnt
        ENDC

; SPI PORT
;outputs
#define SPI_NSS         PORTA,0
#define SPI_MOSI        PORTA,1
#define SPI_CLK         PORTA,2
#define RB2             PORTB,2
; inputs
#define MBUSY           PORTA,3
#define PS_CLK          PORTB,0
#define PS_DAT          PORTB,3

        ORG     0x000           ; Program starts at 0x000
        goto    Init

        ORG     0x04            ; interrupt vector
	movwf   w_temp          ; save W register contents
	movf    STATUS, w       ; move status register into W 
	movwf	status_temp     ; save STATUS register
	
        rlf     ps_data
        bsf     ps_data,0
        btfss   PS_DAT
        bcf     ps_data,0

        movlw   1
        addwf   ps_cnt, f

	BCF     INTCON, INTF    ; CLR IRQ flag
	movf    status_temp,w   ; retrieve STATUS register
	movwf	STATUS          ; restore STATUS register 
	swapf   w_temp,f
	swapf   w_temp,w        ; restore W register 
	RETFIE                  ; return from interrupt

Init
        BCF     STATUS,RP0      ; RAM bank 0
        CLRF    PORTA           ; Initialize port A
        CLRF    PORTB           ; Initialize port B

        BSF     STATUS,RP0      ; RAM bank 1 
        ; 0 - output, 1 -input 
        movlw   b'11111000'     
        movwf   TRISA
        movlw   b'11111011'
        movwf   TRISB
        
        movlw   b'10101010'
        movwf   ps_data

        BSF     INTCON, 7       ; Enable global interrupt
        BSF     INTCON, 4       ; Enable RB0 external interrupt

; FUNCTION OF PORT A PINS
        bcf     STATUS,RP0      ; RAM bank 0
        bcf     STATUS, INTEDG
        MOVLW   7
        MOVWF   CMCON           ; Comparators off, all pins digital I/O

; Init vars
        movlw   b'11111111'     ; Инициализация при старте
        movwf   zx_buttons      ; Кнопки кепмстон мышки
        movwf   zx_cord_x       ; Координата X
        movwf   zx_cord_y       ; Координата Y
        call    spi_tx          ; Загрузим данные в CPLD

; Main loop
Main    
        ;MOVLW   1
        ;addwf   zx_cord_x,f
        ;addwf   zx_cord_y,f
        ;addwf   zx_buttons,f
        movf    ps_cnt, 0
        movwf   zx_cord_y

        movf    ps_data, 0
        movwf   zx_cord_x

        call    spi_tx
        
        bcf     RB2
        call    delay
        bsf     RB2
        call    delay
        ;call    delay
        GOTO    Main

; SPI send data
spi_tx  
        btfss   MBUSY
        goto    spi_tx

        BCF     SPI_NSS
        ; tx zx_cord_y
        BSF     SPI_CLK
        btfsc   zx_cord_y,7
        BSF     SPI_MOSI
        btfss   zx_cord_y,7
        BCF     SPI_MOSI
        BCF     SPI_CLK

        BSF     SPI_CLK
        btfsc   zx_cord_y,6
        BSF     SPI_MOSI
        btfss   zx_cord_y,6
        BCF     SPI_MOSI
        BCF     SPI_CLK

        BSF     SPI_CLK
        btfsc   zx_cord_y,5
        BSF     SPI_MOSI
        btfss   zx_cord_y,5
        BCF     SPI_MOSI
        BCF     SPI_CLK

        BSF     SPI_CLK
        btfsc   zx_cord_y,4
        BSF     SPI_MOSI
        btfss   zx_cord_y,4
        BCF     SPI_MOSI
        BCF     SPI_CLK

        BSF     SPI_CLK
        btfsc   zx_cord_y,3
        BSF     SPI_MOSI
        btfss   zx_cord_y,3
        BCF     SPI_MOSI
        BCF     SPI_CLK

        BSF     SPI_CLK
        btfsc   zx_cord_y,2
        BSF     SPI_MOSI
        btfss   zx_cord_y,2
        BCF     SPI_MOSI
        BCF     SPI_CLK

        BSF     SPI_CLK
        btfsc   zx_cord_y,1
        BSF     SPI_MOSI
        btfss   zx_cord_y,1
        BCF     SPI_MOSI
        BCF     SPI_CLK

        BSF     SPI_CLK
        btfsc   zx_cord_y,0
        BSF     SPI_MOSI
        btfss   zx_cord_y,0
        BCF     SPI_MOSI
        BCF     SPI_CLK
        ; tx zx_cord_x
        BSF     SPI_CLK
        btfsc   zx_cord_x,7
        BSF     SPI_MOSI
        btfss   zx_cord_x,7
        BCF     SPI_MOSI
        BCF     SPI_CLK

        BSF     SPI_CLK
        btfsc   zx_cord_x,6
        BSF     SPI_MOSI
        btfss   zx_cord_x,6
        BCF     SPI_MOSI
        BCF     SPI_CLK

        BSF     SPI_CLK
        btfsc   zx_cord_x,5
        BSF     SPI_MOSI
        btfss   zx_cord_x,5
        BCF     SPI_MOSI
        BCF     SPI_CLK

        BSF     SPI_CLK
        btfsc   zx_cord_x,4
        BSF     SPI_MOSI
        btfss   zx_cord_x,4
        BCF     SPI_MOSI
        BCF     SPI_CLK

        BSF     SPI_CLK
        btfsc   zx_cord_x,3
        BSF     SPI_MOSI
        btfss   zx_cord_x,3
        BCF     SPI_MOSI
        BCF     SPI_CLK

        BSF     SPI_CLK
        btfsc   zx_cord_x,2
        BSF     SPI_MOSI
        btfss   zx_cord_x,2
        BCF     SPI_MOSI
        BCF     SPI_CLK

        BSF     SPI_CLK
        btfsc   zx_cord_x,1
        BSF     SPI_MOSI
        btfss   zx_cord_x,1
        BCF     SPI_MOSI
        BCF     SPI_CLK

        BSF     SPI_CLK
        btfsc   zx_cord_x,0
        BSF     SPI_MOSI
        btfss   zx_cord_x,0
        BCF     SPI_MOSI
        BCF     SPI_CLK

        ; tx zx_buttons
        BSF     SPI_CLK
        btfsc   zx_buttons,7
        BSF     SPI_MOSI
        btfss   zx_buttons,7
        BCF     SPI_MOSI
        BCF     SPI_CLK

        BSF     SPI_CLK
        btfsc   zx_buttons,6
        BSF     SPI_MOSI
        btfss   zx_buttons,6
        BCF     SPI_MOSI
        BCF     SPI_CLK

        BSF     SPI_CLK
        btfsc   zx_buttons,5
        BSF     SPI_MOSI
        btfss   zx_buttons,5
        BCF     SPI_MOSI
        BCF     SPI_CLK

        BSF     SPI_CLK
        btfsc   zx_buttons,4
        BSF     SPI_MOSI
        btfss   zx_buttons,4
        BCF     SPI_MOSI
        BCF     SPI_CLK

        BSF     SPI_CLK
        btfsc   zx_buttons,1
        BSF     SPI_MOSI
        btfss   zx_buttons,1
        BCF     SPI_MOSI
        BCF     SPI_CLK

        BSF     SPI_CLK
        btfsc   zx_buttons,0
        BSF     SPI_MOSI
        btfss   zx_buttons,0
        BCF     SPI_MOSI
        BCF     SPI_CLK
        nop
        BSF     SPI_NSS
        RETURN

; delay 250 mS
delay   MOVLW   250
        MOVWF   Loop1
Outer   MOVLW   200
        MOVWF   Loop2
Inner   NOP
        NOP
        DECFSZ  Loop2,F
        GOTO    Inner          ; Inner loop = 5 usec.
        DECFSZ  Loop1,F
        GOTO    Outer
        RETURN

        END